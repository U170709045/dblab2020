# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
select title
from movies
where budget>10000000 and ranking <6 ;

# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
select distinct title,rating,year 
from movies m
join genres g on m.movie_id = g.movie_id
order by year desc;

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select distinct title,genre_name,duration,oscars
from movies m
join genres g on m.movie_id = g.movie_id
where genre_name="drama" and duration>150 and oscars>2;

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
select title
from movies m
join movie_stars ms on m.movie_id=ms.movie_id
join stars s on ms.star_id=s.star_id
where star_name = "Orlando Bloom" and m.movie_id IN (select movie_id from movie_stars where star_id in (select star_id from stars where star_name ="Ian McKellen")) and oscars>2;

# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.

select distinct title,votes,year,director_name
from movies m
join movie_directors md on md.movie_id=m.movie_id
join directors d on d.director_id=md.director_id;
# 6. Show the thriller films whose budget is greater than 25 million$.	 
select title 
from movies m 
inner join genres g on g.movie_id=m.movie_id
where genre_name="thriller" and budget>25000000;

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
select*
from movies m
join genres g on g.movie_id=m.movie_id
join languages l on l.movie_id=m.movie_id
where language_name="italian" and year between 1990 and 2000 and genre_name="drama";

 # 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 
 select title
 from movies m
 join movie_stars ms on m.movie_id=ms.movie_id
 join stars s on ms.star_id=s.star_id
 where star_name="Tom hanks" and oscars>3;

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select *
from movies m
join genres g on g.movie_id=m.movie_id
join producer_countries pc on pc.movie_id=m.movie_id
join countries c on pc.country_id=c.country_id
where genre_name="history" and country_name="USA" ; 

# 10.Compute the average budget of the films directed by Peter Jackson.
select distinct director_name,title,count(*)
from movies m
join movie_directors md on m.movie_id=md.movie_id
join directors d on d.director_id=md.director_id
group by title;

# 11.Show the Francis Ford Coppola film that has the minimum budget.
select title
from movies m
join movie_directors md on md.movie_id=m.movie_id
join directors d on d.director_id=md.director_id
where director_name="Francis Ford Coppola";




# 12.Show the film that has the most vote and has been produced in USA.